

Questions from solarbuddy attached here: https://trello.com/c/GTwjOP3z/19-load-questions-into-firebase-db

Cleaned questions:
https://docs.google.com/spreadsheets/d/1STQ5dvXoVhteImXn7F-a5ZCg0nAguk2tDrwmxCWes3s/edit?usp=sharing

Instructions:

1. Update your questions/answers
2. Copy the cells *without the header row* and paste it into questions_raw.txt
3. Run read_questions.js:

```shell
./read_questions.js
```

The updated questions will be saved to /react-app/src/questions/questions.ts

Alternatively, you could edit the questions in this file directly.
But if the read_questions script is ever run again it will overwrite them.