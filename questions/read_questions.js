#!/usr/bin/env node

const { readFile, writeFile } = require('fs/promises');

const inputPath = `${__dirname}/questions_raw.txt`;
const outputPath = `${__dirname}/../react-app/src/questions/questions.ts`;

async function main() {
  console.log(`reading from ${inputPath}`);
  const fileContents = (await readFile(inputPath)).toString();

  const questions = fileContents
    .split('\n')
    .filter(Boolean)
    .map((row) => {
      const [section, question, a, b, c, d, answer] = row.split('\t');

      const answerIndex = answer.charCodeAt(0) - 'a'.charCodeAt(0);

      return {
        question,
        section,
        answers: [a, b, c, d],
        answerIndex,
      };
    });

  const fileContent = `export const questions = ${JSON.stringify(
    questions,
    null,
    2
  )};`;

  console.log(`writing output to ${outputPath}`);
  await writeFile(outputPath, fileContent);
  console.log('wrote file successfully');
}

main().catch((error) => {
  console.error(error);
  process.exit(1);
});
