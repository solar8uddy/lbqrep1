# SolarBuddy Firebase Functions

### Pre-requisites
- Firebase CLI
- yarn

To deploy the function:

```
firebase login
yarn deploy
```