import {https, logger, database, config} from "firebase-functions";
import {initializeApp, database as rtdb} from "firebase-admin";
import axios from "axios";
import * as FormData from "form-data"
import * as md5 from "md5";

initializeApp();

type Quiz = {
  eventId: string;
  eventName: string;
  participantsCount: number;
  adminEmails: string[];
};

type SFConfig = {
    host: string;
    clientid: string;
    clientsecret: string;
    serviceuser: {
        name: string;
        password: string;
        accesstoken: string;
    }
}

exports.createOrUpdateQuiz = https.onRequest(async (request, response) => {
  if (request.method !== "POST") {
    response.status(403).send("Forbidden!");
  }

  const quiz: Quiz = {
    eventId: request.body.eventId,
    eventName: request.body.eventName,
    participantsCount: request.body.participants,
    adminEmails: request.body.adminEmails,
  };

  try {
      const {eventId, ... eventData} = quiz;
      await rtdb().ref('events/' + eventId).set(eventData);

      response
        .status(200)
        .send(`Set quiz from event to db: ${JSON.stringify(quiz)}`);
  } catch (e) {
    const errorMessage = `Error saving: ${e}`;

    logger.error(errorMessage, e);
    response.status(500).send(errorMessage);
  }
});

// Hash the users email server side so that we can show gravatar images without distributing it
exports.onUserEmailUpdate = database.ref("/users/{uid}/email").onWrite((change, context) => {
  // Exit when the data is deleted.
  if (!change.after.exists()) {
    return null;
  }

  const email = change.after.val();
  const emailMd5 = md5(email.trim().toLowerCase());

  const userRef = change.after.ref.parent;

  if (!userRef) {
    return null;
  }

  return userRef.child('emailHash').set(emailMd5);
});


exports.onLetterUpdated = database.ref("/letters/{eventId}/{uid}").onUpdate((snapshot, context) => {
    const cfg = config().salesforce as SFConfig;

    const eventId = context.params.eventId;

    const email = snapshot.after.child("email").val();
    const content = snapshot.after.child("content").val();

    logger.info(`email: ${email}`)
    logger.info(`content: ${content}`)

    if(!content || content === '') {
        return '';
    }

    return getToken()
      .then((token) => {
        return axios({
            method: "post",
            url: `https://${cfg.host}/services/apexrest/letter`,
            data: {
                "quizId": eventId,
                "userId": email,
                "content": content,
            },
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
            }
        }).then((resp) => {
            logger.debug(`resp: ${resp.data}`);
        }).catch((e) => {
            logger.error(`error: ${e}`, e.response ? e.response.data : undefined);
        });
    });
});

exports.onLetterCreated = database.ref("/letters/{eventId}/{uid}").onCreate((snapshot, context) => {
    const cfg = config().salesforce as SFConfig;

    const eventId = context.params.eventId;

    const email = snapshot.child("email").val();
    const content = snapshot.child("content").val();

    logger.debug(`email: ${email}`)
    logger.debug(`content: ${content}`)

    if(!content || content === '') {
        return '';
    }

    return getToken()
      .then((token) => {
        return axios({
            method: "post",
            url: `https://${cfg.host}/services/apexrest/letter`,
            data: {
                "quizId": eventId,
                "userId": email,
                "content": content,
            },
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
            }
        }).then((resp) => {
            logger.debug(`resp: ${resp.data}`);
        }).catch((e) => {
            logger.error(`error: ${e}`, e.response ? e.response.data : undefined);
        });
    });
});

// First time a user starts answering questions
exports.onParticipantCreated = database.ref("/events/{eventId}/participants/{uid}").onCreate(async (snapshot, context) => {
    const cfg = config().salesforce as SFConfig;

    const eventId = context.params.eventId;
    const userId = context.params.uid;

    try {
        const userSnapshot = await rtdb().ref(`users/${userId}`).once("value");

        if (!userSnapshot || !userSnapshot.exists()) {
            logger.error(`user ${userId} does not exist`);
            return;
        }

        const user = userSnapshot.val();

        try {
            const token = await getToken();

            const resp = await axios({
                method: "post",
                url: `https://${cfg.host}/services/apexrest/participant`,
                data: {
                    "quizId": eventId,
                    "displayName": user.displayName,
                    "email": user.email,
                    "emailHash": user.emailHash,
                    "uid": user.uid,
                },
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${token}`
                }
            });

            logger.debug(`resp: ${resp.data}`);

            return "";
        } catch (e) {
            logger.error(`error: ${e}`, e.response ? e.response.data : undefined);

            return "";
        }
    } catch (userSnapshotError) {
        logger.error(`User snapshot error: ${userSnapshotError}`, userSnapshotError);

        return "";
    }
});

exports.onAnswerUpdated = database.ref("/answers/{eventId}/{uid}/answers/{questionIndex}").onUpdate(async (snapshot, context) => {
    const cfg = config().salesforce as SFConfig;

    const eventId = context.params.eventId;
    const userId = context.params.uid;

    const answer = snapshot.after.val();

    logger.debug(`userid: ${userId}`)
    logger.debug(`answer: ${JSON.stringify(answer)}`)

    if(!answer || typeof answer !== 'object') {
        return "";
    }

    if (
      typeof answer.index !== 'number' ||
      typeof answer.answer !== 'string' ||
      typeof answer.isCorrect !== 'boolean' ||
      typeof answer.hasAnswered !== 'boolean'
    ) {
        logger.error(`incorrect answer data structure`);
        return;
    }

    if (!answer.hasAnswered) {
        logger.info(`user has not answered this question yet`);
    }

    try {
        const emailSnapshot = await rtdb().ref(`users/${userId}/email`).once("value");

        if (!emailSnapshot || !emailSnapshot.exists()) {
            logger.error(`user ${userId} does not exist`);
            return;
        }

        try {
            const token = await getToken();

            const resp = await axios({
                method: "post",
                url: `https://${cfg.host}/services/apexrest/answer`,
                data: {
                    "quizId": eventId,
                    "userId": emailSnapshot.val(),
                    "questionNumber": `${answer.index}`,
                    "answer": answer.answer,
                    "correctAnswer": answer.isCorrect ? 'Y' : 'N',
                },
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${token}`
                }
            });

            logger.debug(`resp: ${resp.data}`);

            return "";
        } catch (e) {
            logger.error(`error: ${e}`, e.response ? e.response.data : undefined);

            return "";
        }
    } catch (userSnapshotError) {
        logger.error(`User snapshot error: ${userSnapshotError}`, userSnapshotError);

        return "";
    }
});

function getToken(): Promise<string> {
    const cfg = config().salesforce as SFConfig;

    const bodyFormData = new FormData();

    bodyFormData.append("client_id", cfg.clientid);
    bodyFormData.append("client_secret", cfg.clientsecret);
    bodyFormData.append("grant_type", "password");
    bodyFormData.append("username", cfg.serviceuser.name);
    bodyFormData.append("password", cfg.serviceuser.password ); //+ cfg.serviceuser.accesstoken

    return axios.post(`https://${cfg.host}/services/oauth2/token`, bodyFormData, {
        headers: bodyFormData.getHeaders(),
    }).then((authResponse) => {
        logger.debug("got token")

        return authResponse.data.access_token as string;
    })
}