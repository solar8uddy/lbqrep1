# SolarBuddy Salesforce APEX Classes

### How to deploy

- Create the files within this directory in the Salesforce ApexCSIPage i.e. `https://<salesforceURL>/_ui/common/apex/debug/ApexCSIPage`
- Add the Solarbuddy Functions URL to the Salesforce remote site settings in `https://<salesforceURL>/lightning/setup/SecurityRemoteProxy`
