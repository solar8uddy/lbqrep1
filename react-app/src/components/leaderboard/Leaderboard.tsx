import {useLeaderboard} from '../../useFirebase';

import styles from './Leaderboard.module.css'
import Item from "./Item";
import {getAvatarUrl} from "../../services/avatar";

export default function Leaderboard({eventId}: { eventId: string }) {
  const {scores} = useLeaderboard({eventId});

  return (
    <div className={styles.leaderboard}>
      <h2 className={styles.heading}>Leaderboard</h2>

      <ol className={styles.list}>
        {scores.map(([profile, score], index) => (
          <li key={profile.uid}>
            <Item
              avatar={getAvatarUrl(profile.emailHash)}
              name={profile.displayName}
              score={score}
              highlight={index <= 1}
            />
          </li>
        ))}
      </ol>
    </div>
  );
};
