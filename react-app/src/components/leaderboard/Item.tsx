import styles from './Item.module.css'
import Avatar from "../Avatar";

type ItemProps = {
  avatar: string;
  name: string;
  score: number;
  highlight?: boolean;
}

export default function Item(props: ItemProps) {
  return (
    <div className={props.highlight ? styles.highlightedContainer : styles.container}>
      <div className={styles.avatarName}>
        <Avatar url={props.avatar} />
        <span className={styles.name}>{props.name}</span>
      </div>

      <div>
        <span className={styles.score}>{props.score}</span>
      </div>
    </div>
  );
};
