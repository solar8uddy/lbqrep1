import React from 'react';

import styles from './Input.module.css';

type TextInputProps = React.InputHTMLAttributes<HTMLInputElement>

function Input(props: TextInputProps) {
  return (
    <input className={styles.textarea} {...props} />
  );
}

export default Input;