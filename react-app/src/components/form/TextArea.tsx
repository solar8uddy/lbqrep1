import React from 'react';

import styles from './Input.module.css';

type TextAreaProps = React.TextareaHTMLAttributes<HTMLTextAreaElement>

function TextArea(props: TextAreaProps) {
  return (
    <textarea className={styles.textarea} {...props} />
  )
}

export default TextArea;