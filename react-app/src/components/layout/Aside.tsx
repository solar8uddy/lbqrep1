import React from "react";

import styles from './Aside.module.css'

type AsideProps = {
  children: React.ReactNode;
}

export default function Aside(props: AsideProps) {
  return (
    <aside className={styles.aside}>
      {props.children}
    </aside>
  )
}