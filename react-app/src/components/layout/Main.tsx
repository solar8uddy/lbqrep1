import React, {useEffect} from "react";

import styles from './Main.module.css'
import {useEventControlState} from "../../useFirebase";
import {backgrounds} from "../../assets/backgrounds";

type MainProps = {
  eventId?: string;
  children: React.ReactNode;
}

export default function Main(props: MainProps) {
  if (props.eventId) {
    return (
      <QuizBackground eventId={props.eventId}>
        <div className={styles.inner}>
          {props.children}
        </div>
      </QuizBackground>
    );
  }

  return (
    <DefaultBackground>
      <div className={styles.inner}>
        {props.children}
      </div>
    </DefaultBackground>
  )
}

type QuizBackgroundProps = {
  eventId: string;
  children: React.ReactNode;
}

function QuizBackground(props: QuizBackgroundProps) {
  const eventControlState = useEventControlState({eventId: props.eventId});

  if (eventControlState.stage !== 'quiz') {
    return <DefaultBackground>{props.children}</DefaultBackground>;
  }

  const {currentQuestionIndex} = eventControlState;

  return (
    <IndexBackground index={currentQuestionIndex}>
      {props.children}
    </IndexBackground>
  );
}

function IndexBackground(props: { index: number; children: React.ReactNode }) {
  const background = backgrounds[props.index % backgrounds.length];
  const nextBackground = backgrounds[(props.index + 1) % backgrounds.length];

  // Preload the next background image
  useEffect(() => {
    const timeout = setTimeout(() => {
      const image = new Image();
      image.src = nextBackground;
    }, 5000)

    return () => clearTimeout(timeout);
  }, [nextBackground]);

  return (
    <main id="#main" className={styles.main} style={{backgroundImage: `url(${background}`}}>
      {props.children}
    </main>
  );
}

function DefaultBackground(props: { children: React.ReactNode }) {
  return (
    <main id="#main" className={styles.main}>
      {props.children}
    </main>
  );
}