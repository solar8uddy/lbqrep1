import React from "react";
import Header, {Stage} from "./Header";
import styles from './Layout.module.css'
import {useAuth} from "../../auth/useAuth";

type LayoutProps = {
  stage?: Stage;
  children: React.ReactNode;
}

export default function Layout(props: LayoutProps) {
  const {user} = useAuth();

  return (
    <div className={styles.container}>
      {user && <Header stage={props.stage}/>}

      <div className={styles.content}>
        {props.children}
      </div>
    </div>
  )
}