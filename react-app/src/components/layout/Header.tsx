import React, {useEffect, useState} from "react";

import Logo from '../../assets/logo.png';
import styles from './Header.module.css';
import {useAuth} from "../../auth/useAuth";
import Avatar from "../Avatar";
import {getAvatarUrl} from "../../services/avatar";
import Progress from "../progress/Progress";
import ProgressItem from "../progress/ProgressItem";
import md5 from "md5";
import {useUsers} from "../../useFirebase";

export type Stage = "quiz" | "letter" | "donate";

type HeaderProps = {
  stage?: Stage;
}

const stageIndex: Record<Stage, number> = {
  "quiz": 0,
  "letter": 1,
  "donate": 2,
}

export default function Header(props: HeaderProps) {
  const {stage = 'quiz'} = props;

  const [name, setName] = useState('');

  const index = stageIndex[stage];

  const auth = useAuth();
  const {getUser} = useUsers();

  const email = auth.user?.email ?? "";

  useEffect(() => {
    if (auth.user?.uid) {
      getUser(auth.user?.uid).then((profile) => setName(profile.displayName))
    }
  }, [auth.user?.uid, getUser, setName])

  return (
    <div className={styles.root}>
      <div className={styles.left}>
        <img src={Logo} className={styles.logo} alt="SolarBuddy"/>
      </div>

      <div className={styles.right}>

        <div className={styles.progress}>
          <Progress>
            <ProgressItem done={index >= 0}>Quiz</ProgressItem>
            <ProgressItem done={index >= 1}>Letter</ProgressItem>
            <ProgressItem done={index >= 2}>Share</ProgressItem>
          </Progress>
        </div>

        {email && (
          <div className={styles.user}>
            <span className={styles.name}>{name}</span>
            <Avatar url={getAvatarUrl(emailHash(email))}/>
          </div>
        )}
      </div>
    </div>
  )
}

function emailHash(email: string) {
  return md5(email.trim().toLowerCase())
}