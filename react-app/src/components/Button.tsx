import React from 'react';

import styles from './Button.module.css';

export type ButtonProps = React.ButtonHTMLAttributes<HTMLButtonElement>;

function Button(props: ButtonProps) {
  return (
    <button className={styles.button} {...props} />
  );
}

export default Button;