import React from 'react';

import ArrowRight from '../assets/ArrowRight'
import Button, {ButtonProps} from "./Button";
import styles from './ArrowButton.module.css'
import baseStyles from './Button.module.css'

function ArrowButton(props: ButtonProps) {
  const {children, ...rest} = props;

  return (
    <Button {...rest} className={`${baseStyles.button} ${styles.button}`}>
      <span className={styles.wrapper}>
        <span className={styles.children}>{children}</span>
        <span className={styles.arrow}><ArrowRight/></span>
      </span>
    </Button>
  );
}

export default ArrowButton;