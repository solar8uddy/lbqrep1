import React from 'react';
import styles from "./Avatar.module.css";

type AvatarProps = {
  url: string;
}

function Avatar(props: AvatarProps) {
  return (
    <img className={styles.avatar} src={props.url} alt=""/>
  );
}

export default Avatar;