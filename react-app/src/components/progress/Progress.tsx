import React from 'react';

import styles from './Progress.module.css';

type ProgressProps = {
  children: React.ReactNode;
}

function Progress(props: ProgressProps) {
  return (
    <ol className={styles.progress}>
      {props.children}
    </ol>
  );
}

export default Progress;