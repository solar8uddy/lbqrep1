import React from 'react';

import styles from './ProgressItem.module.css';

type ProgressItemProps = {
  done?: boolean;
  children: React.ReactNode;
}

function ProgressItem(props: ProgressItemProps) {
  const classes = [
    styles.item,
    props.done ? styles.done : styles.todo
  ].join(' ')

  return (
    <li className={classes}>
      {props.children}
    </li>
  );
}

export default ProgressItem;