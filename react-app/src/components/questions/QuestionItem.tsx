import React from 'react';
import styles from "./Questions.module.css";
import CheckCircle from "../../assets/CheckCircle";
import CrossCircle from "../../assets/CrossCircle";

type QuestionItemProps = {
  disabled?: boolean;
  selected?: boolean;
  correct?: boolean;
  incorrect?: boolean;
  onClick?: () => void;
  children: React.ReactNode;
}

function QuestionItem(props: QuestionItemProps) {
  return (
    <li>
      <button
        className={`${styles.optionButton} ${props.selected ? styles.optionSelected : ''}`}
        disabled={props.disabled}
        onClick={props.onClick}
      >
        {props.children}

        {(props.correct || props.incorrect) && (
          <span>
            {props.correct ? <CheckCircle/> : <CrossCircle/>}
          </span>
        )}
      </button>
    </li>
  );
}

export default QuestionItem;