import React from 'react';
import styles from "./Questions.module.css";

type QuestionListProps = {
  children: React.ReactNode;
}

function QuestionList(props: QuestionListProps) {
  return (
    <ul className={styles.options}>
      {props.children}
    </ul>
  );
}

export default QuestionList;