import React from 'react';

import styles from './Title.module.css'

type TitleProps = {
  children: React.ReactNode;
  small?: boolean;
}

function Title(props: TitleProps) {
  return (
    <span className={`${styles.title} ${props.small && styles.small}`}>{props.children}</span>
  );
}

export default Title;