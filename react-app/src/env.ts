export const getFirebaseConfig = () => {
  if (process.env.REACT_APP_FIREBASE_ENV === 'prod') {
    return {
      apiKey: 'AIzaSyDjFESuaeUu-oq65DsydRHjPSNt3dIYx8g',
      authDomain: 'solarbuddy-light-build-quiz.firebaseapp.com',
      databaseURL:
        'https://solarbuddy-light-build-quiz-default-rtdb.asia-southeast1.firebasedatabase.app',
      projectId: 'solarbuddy-light-build-quiz',
      storageBucket: 'solarbuddy-light-build-quiz.appspot.com',
      messagingSenderId: '39426403330',
      appId: '1:39426403330:web:1ca39ed3ea342db57a4b1b',
    };
  }

  return {
    apiKey: 'AIzaSyCn6G1VdYlzO4iQyq9fAsXY2d1TRdmV-UE',
    authDomain: 'e4g-solar-buddy-spike.firebaseapp.com',
    databaseURL:
      'https://e4g-solar-buddy-spike-default-rtdb.asia-southeast1.firebasedatabase.app',
    projectId: 'e4g-solar-buddy-spike',
    storageBucket: 'e4g-solar-buddy-spike.appspot.com',
    messagingSenderId: '715632627187',
    appId: '1:715632627187:web:2da12c405a9c99e0961686',
  };
};
