import React, {useEffect, useState} from 'react';
import {questions} from './questions/questions';
import {useAnswerState, useEventControlState} from './useFirebase';
import Title from "./components/quiz/Title";
import styles from './Quiz.module.css';
import {Redirect} from "react-router-dom";
import QuestionList from "./components/questions/QuestionList";
import QuestionItem from "./components/questions/QuestionItem";

const timeLimit = 20;

interface Props {
  eventId: string;
  setScore: (updater: (prevScore: number) => number) => void;
}

// question is zero-indexed [0-21]
// images are one-indexed [1-22]

const numQuestions = questions.length;

interface LoadedQuizProps {
  setAnswer: (answerIndex: number) => void;
  answerIndices: number[];
  currentQuestion: number;
  setScore: (updater: (prevScore: number) => number) => void;
}

export function useScoreCalculator(currentQuestion: number) {
  const [timeStartedCurrentQuestion, setTime] = useState(Date.now());
  useEffect(() => {
    setTime(Date.now());
  }, [currentQuestion]);

  return {
    timeStartedCurrentQuestion,
    getScore: () => {
      const scoreBuckets = [
        [10, 100],
        [20, 90],
        [30, 80],
        [40, 70],
        [50, 60],
        [60, 50],
      ];
      const minimumScore = 10;

      const secondsTaken =
        Date.now() / 1000 - timeStartedCurrentQuestion / 1000;
      const score =
        scoreBuckets.find(([timeThreshold]) => {
          return secondsTaken < timeThreshold;
        })?.[1] ?? minimumScore;
      return score;
    },
  };
}

function useTimeSince(start: number) {
  const [, setTime] = useState(Date.now());

  useEffect(() => {
    // every second, refresh the display
    const timeout = window.setInterval(() => {
      setTime(Date.now());
    }, 1000);

    return () => {
      window.clearInterval(timeout);
    };
  }, [start]);

  return Math.round(Date.now() / 1000 - start / 1000);
}

function ScoreClock({
                      timeStartedCurrentQuestion,
                      answeredCorrectly,
                    }: {
  timeStartedCurrentQuestion: number;
  answeredCorrectly: boolean | null;
}) {
  const [, setTime] = useState(Date.now());
  useEffect(() => {
    // if (answeredCorrectly !== null) {
    //   return;
    // }
    // every second, refresh the display
    const timeout = window.setInterval(() => {
      setTime(Date.now());
    }, 1000);

    return () => {
      window.clearInterval(timeout);
    };
  }, [timeStartedCurrentQuestion, answeredCorrectly]);

  const secondsTaken = Math.round(
    Date.now() / 1000 - timeStartedCurrentQuestion / 1000
  );

  const secondsRemaining = Math.max(timeLimit - secondsTaken, 0)

  // if (answeredCorrectly !== null) {
  //   return null;
  // }

  if (secondsTaken > timeLimit) {
    return null;
  }

  return <div className={styles.timer}>
    <span>{secondsRemaining}</span>
  </div>;

  // if (answeredCorrectly === null) {
  //   return (
  //     <div>
  //       <div>Time taken so far: {secondsTaken}</div>
  //       <div>Points available: {getScore()}</div>
  //     </div>
  //   );
  // } else if (answeredCorrectly) {
  //   return <div>You got it right! Waiting for host to proceed</div>;
  // } else {
  //   return <div>Aww, that's not the right answer</div>;
  // }
}

const LoadedQuiz = ({
                      currentQuestion,
                      answerIndices,
                      setAnswer,
                      setScore,
                    }: LoadedQuizProps) => {
  const question = questions[currentQuestion];

  const {timeStartedCurrentQuestion, getScore} =
    useScoreCalculator(currentQuestion);

  const secondsTaken = useTimeSince(timeStartedCurrentQuestion);

  const isOutOfTime = secondsTaken > timeLimit;

  if (!question) {
    return <Redirect to={{pathname: `/`}}/>
  }

  const hasAnsweredCurrentQuestion = answerIndices[currentQuestion] !== -1;
  const answeredCorrectly = hasAnsweredCurrentQuestion
    ? answerIndices[currentQuestion] === questions[currentQuestion]?.answerIndex
    : null;

  return (
    <div className={styles.wrapper}>
      <div>
        <Title small>{question.question}</Title>

        <QuestionList>
          {question.answers.map((answer, i) => (
            <QuestionItem
              key={answer}
              selected={i === answerIndices[currentQuestion]}
              disabled={hasAnsweredCurrentQuestion}
              correct={Boolean(hasAnsweredCurrentQuestion && isOutOfTime && i === questions[currentQuestion]?.answerIndex)}
              incorrect={Boolean(hasAnsweredCurrentQuestion && isOutOfTime && i === answerIndices[currentQuestion] && !answeredCorrectly)}
              onClick={() => {
                if (hasAnsweredCurrentQuestion) {
                  return;
                }
                if (i === question.answerIndex) {
                  setScore((score) => score + getScore());
                }
                setAnswer(i);
              }}
            >
              {answer}
            </QuestionItem>
          ))}
        </QuestionList>

        <ScoreClock
          timeStartedCurrentQuestion={timeStartedCurrentQuestion}
          answeredCorrectly={answeredCorrectly}
        />
      </div>

      <div>Quiz challenge Q{currentQuestion + 1} of {numQuestions} - {question.section}</div>
    </div>
  );
};

export const Quiz = ({setScore, eventId}: Props) => {
  const {answerState, setAnswerState} = useAnswerState({eventId});
  const eventControlState = useEventControlState({eventId});

  if (!answerState.hasLoaded || eventControlState.stage !== 'quiz') {
    return null;
  }
  const {currentQuestionIndex} = eventControlState;
  if (currentQuestionIndex >= numQuestions) {
    return (
      <div>
        <p>The quiz is now done. Waiting for event host to proceed.</p>
      </div>
    );
  }

  return (
    <LoadedQuiz
      setAnswer={(answerIndex) => {
        const newAnswers = [...answerState.answerIndices];
        newAnswers[currentQuestionIndex] = answerIndex;
        setAnswerState(newAnswers);
      }}
      setScore={setScore}
      currentQuestion={currentQuestionIndex}
      answerIndices={answerState.answerIndices}
    />
  );
};
