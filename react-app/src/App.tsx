import {
  BrowserRouter as Router,
  Redirect,
  Switch,
  Route,
  RouteProps,
} from 'react-router-dom';

import 'modern-normalize/modern-normalize.css';

import {CompleteSignInPage, LoginPage, SetNamePage} from './auth/auth';
import {ProvideAuth, useAuth} from './auth/useAuth';
import {
  EventPage,
  EventQuizPage,
  EventLetterWritingPage,
} from './event/eventPage';
import {AdminEventPage} from './admin/adminEventPage';
import {DonatePage} from './donate/donatePage';

function App() {
  return (
    <ProvideAuth>
      <Router>
        {/*<AuthButton/>*/}
        {/* A <Switch> looks through its children <Route>s and
          renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/login">
            <LoginPage/>
          </Route>
          <Route path="/completeSignIn">
            <CompleteSignInPage/>
          </Route>
          <Route path="/name">
            <SetNamePage/>
          </Route>
          <PrivateRoute path="/event/:eventId" exact>
            <EventPage/>
          </PrivateRoute>
          <PrivateRoute path="/event/:eventId/quiz">
            <EventQuizPage/>
          </PrivateRoute>
          <PrivateRoute path="/event/:eventId/letter-writing">
            <EventLetterWritingPage/>
          </PrivateRoute>
          <PrivateRoute path="/event/:eventId/admin">
            <AdminEventPage/>
          </PrivateRoute>
          <Route path="/donate">
            <DonatePage/>
          </Route>
          <Route>
            <Redirect to="/login"/>
          </Route>
        </Switch>
      </Router>
    </ProvideAuth>
  );
}

// A wrapper for <Route> that redirects to the login
// screen if you're not yet authenticated.
function PrivateRoute({children, ...rest}: RouteProps) {
  let auth = useAuth();

  if (auth.isLoading) {
    return null;
  }
  
  return (
    <Route
      {...rest}
      render={({location}) =>
        auth.user ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: `/login`,
              search: new URLSearchParams({
                target: location.pathname,
              }).toString(),
            }}
          />
        )
      }
    />
  );
}

export default App;
