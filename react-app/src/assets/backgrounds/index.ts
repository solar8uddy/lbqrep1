import background1 from './background-1.png';
import background2 from './background-2.png';
import background3 from './background-3.png';
import background4 from './background-4.png';
import background5 from './background-5.png';
import background6 from './background-6.png';
import background7 from './background-7.png';
import background8 from './background-8.png';
import background9 from './background-9.png';
import background10 from './background-10.png';
import background11 from './background-11.png';
import background12 from './background-12.png';
import background13 from './background-13.png';
import background14 from './background-14.png';
import background15 from './background-15.png';
import background16 from './background-16.png';
import background17 from './background-17.png';
import background18 from './background-18.png';
import background19 from './background-19.png';
import background20 from './background-20.png';
import background21 from './background-21.png';
import background22 from './background-22.png';

export const backgrounds = [
  background1,
  background2,
  background3,
  background4,
  background5,
  background6,
  background7,
  background8,
  background9,
  background10,
  background11,
  background12,
  background13,
  background14,
  background15,
  background16,
  background17,
  background18,
  background19,
  background20,
  background21,
  background22,
];