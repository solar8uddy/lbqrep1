export const questions = [
  {
    "question": "How many people are impacted by extreme energy poverty globally?",
    "section": "Energy Poverty",
    "answers": [
      "7.89 Billion",
      "789 Million",
      "78.9 Million",
      "7.89 Million"
    ],
    "answerIndex": 1
  },
  {
    "question": "As a percentage how many people are affected by the impact of extreme energy poverty?",
    "section": "Energy Poverty",
    "answers": [
      "1% (1 in 100)",
      "2% (2 in 100)",
      "5% (5 in 100)",
      "10% (1 in 10)"
    ],
    "answerIndex": 3
  },
  {
    "question": "As per the Time Magazine article in 2011 what is the worst form of poverty?",
    "section": "Energy Poverty",
    "answers": [
      "Energy Poverty",
      "Light pollution",
      "Energy waste",
      "Remore poverty"
    ],
    "answerIndex": 0
  },
  {
    "question": "How many tons of C02 are produced by kerosene lanterns annually",
    "section": "Energy Poverty",
    "answers": [
      "100 million tones",
      "190 million tones",
      "1.28 million tones",
      "128 million tones"
    ],
    "answerIndex": 1
  },
  {
    "question": "As a percentage how much of their annual income do families living in extreme energy poverty spend on fuel for lighting?",
    "section": "Energy Poverty",
    "answers": [
      "Up to 5%",
      "Up to 25%",
      "Up to 40%",
      "Up to 1%"
    ],
    "answerIndex": 2
  },
  {
    "question": "How many deaths are caused by indoor air pollution annually?",
    "section": "Energy Poverty",
    "answers": [
      "400,000",
      "4 Million",
      "4,000",
      "40 million"
    ],
    "answerIndex": 1
  },
  {
    "question": "One SolarBuddy solar light helps offset CO2 emissions cause by kerosene and other toxic fuel for lighting. How many tones of CO2 are offset per light?",
    "section": "Impact",
    "answers": [
      "0.28 tones",
      "0.58 tones",
      "1.28 tones",
      "1.58 tones"
    ],
    "answerIndex": 2
  },
  {
    "question": "One SolarBuddy solar light helps offset CO2 emissions cause by kerosene and other toxic fuel for lighting. How many trees would need to be planted to offset the same amount of CO2?",
    "section": "Impact",
    "answers": [
      "15.3 trees",
      "21.3 trees",
      "12.3 trees",
      "13.5 trees"
    ],
    "answerIndex": 1
  },
  {
    "question": "Families who recieve a solar light have an 80% reduction in kerosene expenditure. What is the equivalent number of weeks wages a family living in energy poverty would save as a result.",
    "section": "Impact",
    "answers": [
      "16 weeks",
      "32 weeks",
      "2 weeks",
      "6 weeks"
    ],
    "answerIndex": 0
  },
  {
    "question": "Each SolarBuddy light impacts many more people than just the child recieving the solar light. If 100 lights were gifted to 100 children how many lives would be impacted as a result",
    "section": "Impact",
    "answers": [
      "100 (1 live per light)",
      "200 (2 lives per light)",
      "400 (4 lives per light)",
      "500 (5 lives per light)"
    ],
    "answerIndex": 3
  },
  {
    "question": "What is the average lifespan of a SolarBuddy solar light?",
    "section": "Light",
    "answers": [
      "6-12 months",
      "2-3 years",
      "6-7 years",
      "10-15 years"
    ],
    "answerIndex": 3
  },
  {
    "question": "Which is NOT correct about SolarBuddy's JuniorBuddy light",
    "section": "Light",
    "answers": [
      "Easy to assemble",
      "Designed with purpose",
      "Has eyes, arms and feet",
      "Has an inbuilt camera"
    ],
    "answerIndex": 3
  },
  {
    "question": "How many study hours are created over the lifespan of a SolarBuddy light?",
    "section": "Light",
    "answers": [
      "7300 hours",
      "5300 hours",
      "300 hours",
      "4321 hours"
    ],
    "answerIndex": 0
  },
  {
    "question": "How many Sustainable Development Goals are there?",
    "section": "SDG",
    "answers": [
      "12",
      "20",
      "17",
      "8"
    ],
    "answerIndex": 2
  },
  {
    "question": "Which of the below are NOT a UN SDG",
    "section": "SDG",
    "answers": [
      "Partnerships for the Goals",
      "No Poverty",
      "World Peace",
      "Clean Water and Sanitisation"
    ],
    "answerIndex": 2
  },
  {
    "question": "SolarBuddy has two goals, its first being to gift lights to children living in energy poverty. How many lights so SolarBuddy want to gift by 2030?",
    "section": "SolarBuddy",
    "answers": [
      "6 Million",
      "600,000",
      "160,000",
      "1.6 Million"
    ],
    "answerIndex": 0
  },
  {
    "question": "SolarBuddy has two goals, its second being to educate and inspire active agents of change in ending energy poverty? How many lives do SolarBuddy want to educate and inspire by 2030?",
    "section": "SolarBuddy",
    "answers": [
      "12 Million",
      "6 Million",
      "2.4 Million",
      "1.2 Million"
    ],
    "answerIndex": 1
  },
  {
    "question": "How many countries has SolarBuddy been active in since founded?",
    "section": "SolarBuddy",
    "answers": [
      "5",
      "8",
      "15",
      "22"
    ],
    "answerIndex": 3
  },
  {
    "question": "How many countries overseas do SolarBuddy have a key focus on right now?",
    "section": "SolarBuddy",
    "answers": [
      "1",
      "4",
      "7",
      "10"
    ],
    "answerIndex": 2
  },
  {
    "question": "How many lights have SolarBuddy distributed in the past 5 years?",
    "section": "SolarBuddy",
    "answers": [
      "50,000",
      "125,000",
      "75,000",
      "100,000"
    ],
    "answerIndex": 1
  },
  {
    "question": "How many lives have SolarBuddy lights impacted in the past 5 years?",
    "section": "SolarBuddy",
    "answers": [
      "500,000",
      "625,000",
      "750,000",
      "250,000"
    ],
    "answerIndex": 1
  },
  {
    "question": "Where is SolarBuddy's Global Head Office",
    "section": "SolarBuddy",
    "answers": [
      "Brisbane",
      "New York",
      "London",
      "Sydney"
    ],
    "answerIndex": 0
  }
];