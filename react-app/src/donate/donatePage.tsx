import React, { useState } from "react";
import Layout from "../components/layout/Layout";
import Title from "../components/quiz/Title";
import Main from "../components/layout/Main";
import buttonStyles from "../components/Button.module.css";
import facebook from '../assets/social/facebook.svg';
import instagram from '../assets/social/instagram.svg';
import twitter from '../assets/social/twitter.svg';
import linkedin from '../assets/social/linkedin.svg';
import youtube from '../assets/social/youtube.svg';

import styles from './DonatePage.module.css';

export const DonatePage = () => {
  const [isDonateClicked, setIsDonateClicked] = useState(false);

  return (
    <Layout stage="donate">
      <Main>
        {!isDonateClicked && <Title>
          Thank you for being a part of this journey...<br/>
          Before you go, help us with a small donation and by sharing your experience on social media.
        </Title>}

        {isDonateClicked && <Title>Thank you!</Title>}

        <div className={styles.buttonWrapper}>
          <a className={buttonStyles.button}
             href="https://donate.solarbuddy.org"
             target="_blank"
             rel="noopener"
             onClick={() => setIsDonateClicked(true)}
          >
            Donate
          </a>
        </div>

        <SocialLinks />
      </Main>
    </Layout>
  );
};

function SocialLinks() {
  return (
    <div className={styles.socialWrapper}>
      <a href="https://www.facebook.com/solarbuddy.org/" target="_blank" rel="noopener">
        <img src={facebook} alt="Facebook"/>
      </a>
      <a href="https://www.instagram.com/solar_buddy/" target="_blank" rel="noopener">
        <img src={instagram} alt="Instagram"/>
      </a>
      <a href="https://twitter.com/solarbuddyorg" target="_blank" rel="noopener">
        <img src={twitter} alt="Twitter"/>
      </a>
      <a href="https://au.linkedin.com/company/solarbuddyaustralia" target="_blank" rel="noopener">
        <img src={linkedin} alt="LinkedIn"/>
      </a>
      <a href="https://www.youtube.com/channel/UC0LbCahQK_HeDU85wFSalLg" target="_blank" rel="noopener">
        <img src={youtube} alt="YouTube"/>
      </a>
    </div>
  );
}