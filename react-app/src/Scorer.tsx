import { useEffect, useState } from 'react';

import { useLeaderboard } from './useFirebase';
import { useAuth } from './auth/useAuth';

import { Quiz } from './Quiz';

const InnerScorer = ({
  eventId,
  initialScore,
  setFirebaseScore,
  uid,
}: {
  eventId: string;
  initialScore: number;
  uid: string;
  setFirebaseScore: ({ uid, score }: { uid: string; score: number }) => void;
}) => {
  const [score, setScore] = useState(initialScore);

  useEffect(() => {
    setFirebaseScore({ uid, score });
  }, [uid, score, setFirebaseScore]);
  return (
    <Quiz eventId={eventId} setScore={setScore} />
  );
};

export const Scorer = ({ eventId }: { eventId: string }) => {
  const auth = useAuth();
  const user = auth.user!;
  const { setScore: setFirebaseScore, getScore } = useLeaderboard({ eventId });

  const [initialScore, setInitialScore] = useState<number | undefined>();
  useEffect(() => {
    getScore({ uid: user.uid! }).then((score) => {
      setInitialScore(score ?? 0);
    });
  }, [getScore, user.uid]);

  if (initialScore === undefined) {
    return null;
  }
  return (
    <InnerScorer
      eventId={eventId}
      setFirebaseScore={setFirebaseScore}
      uid={user.uid}
      initialScore={initialScore}
    />
  );
};
