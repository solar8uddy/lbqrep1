import {useHistory, useRouteMatch} from 'react-router';
import {Redirect} from 'react-router-dom';
import {Scorer} from '../Scorer';

import {useLocalStorageState} from '../useLocalStorageState';

import {
  useEvent,
  Event,
  useEventControlState,
  useLetterState, useLeaderboard,
} from '../useFirebase';
import Leaderboard from "../components/leaderboard/Leaderboard";
import Main from "../components/layout/Main";
import Aside from "../components/layout/Aside";
import React, {useEffect} from "react";
import Title from "../components/quiz/Title";
import Layout from "../components/layout/Layout";
import TextArea from "../components/form/TextArea";
import Button from "../components/Button";

import styles from './EventPage.module.css';
import {useAuth} from "../auth/useAuth";

/**
 * Check that the event ID is in firebase
 *
 * "register" the current user ID to the event to count against the event quota
 *
 * return: jsx to render when error/loading, or Event
 */
function useEventRegistration():
  | {
  showJsx: true;
  jsx: JSX.Element | null;
}
  | { showJsx: false; event: Event } {
  const {
    params: {eventId},
  } = useRouteMatch<{ eventId: string }>();
  const event = useEvent({eventId});

  if (event.isLoading) {
    // Loading time is pretty quick - no need to show a spinner
    return {jsx: <></>, showJsx: true};
  }
  if (!event.event) {
    return {
      jsx: <div>Error: No such event. Double check your event link</div>,
      showJsx: true,
    };
  }
  if (event.event.isAdmin) {
    return {
      showJsx: true,
      jsx: <Redirect to={{pathname: `/event/${eventId}/admin`}}/>,
    };
  }
  return {
    showJsx: false,
    event: event.event,
  };
}

const RequireEventStage = ({
                             eventId,
                             requiredStage,
                             children,
                           }: {
  eventId: string;
  requiredStage: 'quiz' | 'intro-screen';
  children: React.ReactNode;
}) => {
  const controlState = useEventControlState({eventId});
  if (controlState.stage !== requiredStage) {
    return {
      loading: null,
      'intro-screen': <Redirect to={{pathname: `/event/${eventId}`}}/>,
      quiz: <Redirect to={{pathname: `/event/${eventId}/quiz`}}/>,
      outro: <Redirect to={{pathname: `/event/${eventId}/letter-writing`}}/>,
    }[controlState.stage];
  }

  return <>{children}</>;
};

/**
 * EventPage is the "waiting" screen before the quiz
 *
 * route: /event/:eventId
 */
export const EventPage = () => {
  const eventRegistration = useEventRegistration();
  if (eventRegistration.showJsx) {
    return eventRegistration.jsx;
  }
  const {
    event: {eventId},
  } = eventRegistration;
  return (
    <RequireEventStage eventId={eventId} requiredStage="intro-screen">
      <Layout stage="quiz">
        <Main>
          <Title>Waiting for the host to begin the quiz.</Title>
          <SetInitialScore eventId={eventId} />
        </Main>
        <Aside>
          <Leaderboard eventId={eventId}/>
        </Aside>
      </Layout>
    </RequireEventStage>
  );
};

const SetInitialScore =  ({ eventId }: { eventId: string }) => {
  const auth = useAuth();
  const { setScore } = useLeaderboard({ eventId });

  useEffect(() => {
    if (auth.user?.uid) {
      setScore({uid: auth.user?.uid, score: 0});
    }
  }, [auth.user?.uid, setScore]);

  return null;
}

/**
 * EventQuizPage is the quiz screen with questions
 *
 * route: /event/:eventId/quiz
 */
export const EventQuizPage = () => {
  const eventRegistration = useEventRegistration();
  if (eventRegistration.showJsx) {
    return eventRegistration.jsx;
  }
  const {
    event: {eventId},
  } = eventRegistration;
  return (
    <RequireEventStage eventId={eventId} requiredStage="quiz">
      <Layout stage="quiz">
        <Main eventId={eventId} >
          <Scorer eventId={eventId}/>
        </Main>
        <Aside>
          <Leaderboard eventId={eventId}/>
        </Aside>
      </Layout>
    </RequireEventStage>
  );
};

const InnerLetterWritingPage = ({eventId}: { eventId: string }) => {
  // remote firebase state
  const {letterState, setLetterState} = useLetterState({eventId});
  const {replace} = useHistory();

  // local form field state
  const [text, setText] = useLocalStorageState(
    'letter',
    letterState.loading ? '' : letterState.content
  );
  if (letterState.loading) {
    return null;
  }
  // const hasChangedText = text !== letterState.content;
  return (
    <Layout stage="letter">
      <Main>
        <Title>Leave a message for the kids receiving a Solarbuddy light.</Title>

          <form
              onSubmit={(e) => {
                  e.preventDefault();

                  setLetterState({
                      content: text,
                      loading: false,
                      email: letterState.email
                  });

                  replace('/donate')
              }}
          >
          <div className={styles.letterTextarea}>
            <TextArea
              name="letter"
              value={text}
              style={{maxWidth: 800}}
              rows={4}
              onChange={(e) => setText(e.target.value)}
            />

            <div>Or leave empty to move forward without writing a letter</div>
          </div>

          <Button type="submit">Next</Button>
        </form>
      </Main>
    </Layout>
  );
};
/**
 * EventQuizPage is the letter-writing screen
 *
 * route: /event/:eventId/letter-writing
 */
export const EventLetterWritingPage = () => {
  const eventRegistration = useEventRegistration();
  if (eventRegistration.showJsx) {
    return eventRegistration.jsx;
  }
  const {
    event: {eventId},
  } = eventRegistration;

  return <InnerLetterWritingPage eventId={eventId}/>;
};
