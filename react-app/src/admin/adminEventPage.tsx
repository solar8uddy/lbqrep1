import React, {useEffect, useState} from "react";
import {useRouteMatch} from 'react-router';
import {Redirect} from 'react-router-dom';

import {
  useEvent,
  Event,
  useAdminControls,
  EventControlStateIntroScreen,
  SetEventControlState,
  EventControlStateQuiz, EventControlStateOutro
} from '../useFirebase';

import {questions} from '../questions/questions';
import Main from "../components/layout/Main";
import Aside from "../components/layout/Aside";
import Leaderboard from "../components/leaderboard/Leaderboard";
import Layout from "../components/layout/Layout";
import Title from "../components/quiz/Title";
import styles from './AdminEventPage.module.css';
import ArrowButton from "../components/ArrowButton";
import QuestionItem from "../components/questions/QuestionItem";
import QuestionList from "../components/questions/QuestionList";
import {useScoreCalculator} from "../Quiz";

const numQuestions = questions.length;

function useAdminEventRegistration():
  | {
  showJsx: true;
  jsx: JSX.Element | null;
}
  | { showJsx: false; event: Event } {
  const {
    params: {eventId},
  } = useRouteMatch<{ eventId: string }>();
  const event = useEvent({eventId});

  if (event.isLoading) {
    // Loading time is pretty quick - no need to show a spinner
    return {jsx: <></>, showJsx: true};
  }

  if (!event.event) {
    return {
      jsx: <div>Error: No such event. Double check your event link</div>,
      showJsx: true,
    };
  }

  // Boot the participant back to the regular event page if they're not an admin
  if (!event.event.isAdmin) {
    return {
      showJsx: true,
      jsx: <Redirect to={{pathname: `/event/${eventId}`}}/>,
    };
  }

  return {
    showJsx: false,
    event: event.event,
  };
}

/**
 * AdminEventPage is the single screen where the admin controls the whole experience.
 *
 * route: /event/:eventId/admin
 */
export const AdminEventPage = () => {
  const eventRegistration = useAdminEventRegistration();

  if (eventRegistration.showJsx) {
    return eventRegistration.jsx;
  }
  const {
    event: {eventId},
  } = eventRegistration;
  return <InnerAdminEventPage eventId={eventId}/>;
};

const InnerAdminEventPage = ({eventId}: { eventId: string }) => {
  const {eventControlState, setEventControlState} = useAdminControls({
    eventId,
  });

  function getStage() {
    const stage = eventControlState.stage;

    if (stage === 'loading' || stage === 'intro-screen') {
      return 'quiz';
    }

    if (stage === 'outro') {
      return 'letter';
    }

    return stage;
  }

  return (
    <Layout stage={getStage()}>
      <Main eventId={eventId}>
        {eventControlState.stage === 'loading' && <LoadingScreen/>}

        {eventControlState.stage === 'intro-screen' &&
        <IntroScreen state={eventControlState} setState={setEventControlState}/>}

        {eventControlState.stage === 'quiz' &&
        <QuizScreen state={eventControlState} setState={setEventControlState}/>}

        {eventControlState.stage === 'outro' &&
        <OutroScreen state={eventControlState} setState={setEventControlState}/>}
      </Main>
      <Aside>
        <Leaderboard eventId={eventId}/>
      </Aside>
    </Layout>
  );
};

function LoadingScreen() {
  return (
    <div>
      loading...
    </div>
  );
}

type IntroScreenProps = {
  state: EventControlStateIntroScreen;
  setState: SetEventControlState;
}

function IntroScreen(props: IntroScreenProps) {
  const { setState } = props;

  function startQuiz() {
    setState({
      stage: 'quiz',
      currentQuestionIndex: 0,
    })
  }

  return (
    <div>
      <Title>Waiting for the host to begin the quiz.</Title>

      <div>
        <iframe
          className={styles.introVideo}
          width="560"
          height="315"
          src="https://www.youtube.com/embed/k6FpUew1rP0"
          title="YouTube video player"
          frameBorder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen
        />
      </div>

      <ArrowButton onClick={startQuiz}>
        First question
      </ArrowButton>
    </div>
  );
}

type QuizScreenProps = {
  state: EventControlStateQuiz;
  setState: SetEventControlState;
}

function QuizScreen(props: QuizScreenProps) {
  const {state, setState} = props;

  const question = questions[state.currentQuestionIndex];

  const {timeStartedCurrentQuestion} = useScoreCalculator(state.currentQuestionIndex);

  return (
    <div className={styles.wrapper}>
      <div>
        <Title small>{question.question}</Title>

        <QuestionList>
          {question.answers.map((answer, i) => (
            <QuestionItem
              key={answer}
              disabled={true}
              correct={i === question.answerIndex}
            >
              {answer}
            </QuestionItem>
          ))}
        </QuestionList>

        <ArrowButton
          onClick={() => {
            if (state.currentQuestionIndex + 1 < numQuestions) {
              setState({
                stage: 'quiz',
                currentQuestionIndex:
                  state.currentQuestionIndex + 1,
              });
            } else {
              setState({stage: 'outro'});
            }
          }}
        >
          Next Question
        </ArrowButton>

        <AdminScoreClock timeStartedCurrentQuestion={timeStartedCurrentQuestion} />
      </div>

      <div>Quiz challenge Q{state.currentQuestionIndex + 1} of {numQuestions} - {question.section}</div>
    </div>
  );
}

function AdminScoreClock({timeStartedCurrentQuestion}: { timeStartedCurrentQuestion: number; }) {
  const [, setTime] = useState(Date.now());

  useEffect(() => {
    // every second, refresh the display
    const timeout = window.setInterval(() => {
      setTime(Date.now());
    }, 1000);

    return () => {
      window.clearInterval(timeout);
    };
  }, [setTime]);

  const secondsTaken = Math.round(
    Date.now() / 1000 - timeStartedCurrentQuestion / 1000
  );

  const timeLimit = 20;

  const secondsRemaining = Math.max(timeLimit - secondsTaken, 0)

  if (secondsTaken > timeLimit) {
    return null;
  }

  return <div className={styles.timer}>
    <span>{secondsRemaining}</span>
  </div>;
}

type OutroScreenProps = {
  state: EventControlStateOutro;
  setState: SetEventControlState;
}

function OutroScreen(props: OutroScreenProps) {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const {state, setState} = props;

  return (
    <div>
      <Title>Quiz complete</Title>
      <p>Participants have been presented with an opportunity to write a letter and donate</p>

      <div>
        <iframe
          className={styles.introVideo}
          width="560"
          height="315"
          src="https://www.youtube.com/embed/Rz4-4mmpcSY"
          title="YouTube video player"
          frameBorder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen
        />
      </div>
    </div>
  );
}
