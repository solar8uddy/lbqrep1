import { useEffect, useState } from 'react';

import { useCallbackOne, useMemoOne } from 'use-memo-one';
import sortBy from 'lodash/sortBy';

import firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/auth';

import { useAuth } from './auth/useAuth';

import { questions } from './questions/questions';

import { getFirebaseConfig } from './env';

const numQuestions = questions.length;

firebase.initializeApp(getFirebaseConfig());

const useFirebaseRef = (path: string) => {
  const firebaseRef = useMemoOne(() => {
    return firebase.database().ref(path);
  }, [path]);
  return firebaseRef;
};

const useNullableFirebaseRef = (
  path: string,
  ...test: (string | undefined)[]
) => {
  const firebaseRef = useMemoOne(() => {
    // if any of `test` is nullable, return return null
    if (test.some((x) => !x)) return null;
    return firebase.database().ref(path);
  }, [path, ...test]);
  return firebaseRef;
};

// Profile fields that can be publicly read
export interface Profile {
  displayName: string;
  emailHash: string;
  uid: string;
}

// Profile fields that can be written to
export interface WriteProfile {
  displayName: string;
  email: string;
  uid: string;
}

export const useUsers = () => {
  // Global set of users - mirroring those in firebase auth
  const usersRef = useFirebaseRef('/users');
  return useMemoOne(
    () => ({
      getUser: async (uid: string): Promise<Profile> => {
        return (await usersRef.child(uid).get()).val();
      },
      setProfile: async (uid: string, profile: Partial<WriteProfile>): Promise<void> => {
        await usersRef.child(uid).update(profile);
      },
    }),
    [usersRef]
  );
};

export type AnswerState =
  | {
      hasLoaded: false;
    }
  | {
      hasLoaded: true;
      answerIndices: number[];
      answers: Record<number, {
        index: number;
        question: string;
        hasAnswered: boolean;
        answer: string;
        isCorrect: boolean;
      }>;
    };

export interface Event {
  eventId: string;
  numberOfParticipants: number;
  // computed field from `adminEmails` and the current user
  isAdmin: boolean;
  adminEmails: string[];
}

interface FirebaseEvent {
  numberOfParticipants: number;
  adminEmails: string[];
}

type EventState =
  | {
      isLoading: true;
    }
  | {
      isLoading: false;
      event: Event | null;
    };

export function useEvent({ eventId }: { eventId?: string }): EventState {
  const auth = useAuth();
  const uid = auth.user!.uid!;
  const email = auth.user!.email!;

  const [eventState, setEventState] = useState<EventState>({
    isLoading: true,
  });
  const eventRef = useNullableFirebaseRef(`/events/${eventId}`, eventId);

  useEffect(() => {
    if (!eventRef || !eventId) {
      setEventState({
        isLoading: false,
        event: null,
      });
      return;
    }
    eventRef.on('value', (snapshot) => {
      const fbEvent: FirebaseEvent | null = snapshot.val();

      console.log({ fbEvent });

      if (!fbEvent) {
        setEventState({
          isLoading: false,
          event: null,
        });
        return;
      }

      const isAdmin = fbEvent.adminEmails.includes(email);
      if (isAdmin) {
        setEventState({
          isLoading: false,
          event: {
            eventId,
            numberOfParticipants: fbEvent.numberOfParticipants,
            adminEmails: [...fbEvent.adminEmails].sort(),
            isAdmin: true,
          },
        });
        return;
      }

      eventRef.child(`participants/${uid}`).set(true, (error) => {
        if (error) {
          console.error(error);
          // TODO handle error when participant limit is reached
        } else {
          console.log('success');
          setEventState({
            isLoading: false,
            event: {
              eventId,
              numberOfParticipants: fbEvent.numberOfParticipants,
              adminEmails: [...fbEvent.adminEmails].sort(),
              isAdmin: false,
            },
          });
        }
      });
    });

    // TODO unsubscribe
  }, [eventRef, eventId, uid, email]);

  return eventState;
}

export const useLeaderboard = ({ eventId }: { eventId: string }) => {
  const [scores, setScores] = useState<[Profile, number][]>(() => {
    return [];
  });
  const users = useUsers();
  const leaderboardRef = useFirebaseRef(`/leaderboard/${eventId}`);
  useEffect(() => {
    leaderboardRef
      .orderByValue()
      .limitToLast(10)
      .on('value', async (snapshot) => {
        const snapshotVal = snapshot.val() as { [name: string]: number } | null;
        console.log({ snapshot: snapshotVal });
        if (!snapshotVal) {
          return setScores([]);
        }
        // highest score first
        const sortedScores = sortBy(
          Object.entries(snapshotVal),
          ([, val]) => -val
        );

        const profilesWithNames: [Profile, number][] = await Promise.all(
          sortedScores.map(([uid, score]): Promise<[Profile, number]> => {
            console.log({ uid });
            return users.getUser(uid).then((profile) => [profile, score]);
          })
        );
        setScores(profilesWithNames);
      });
  }, [leaderboardRef, users]);

  return {
    scores,
    getScore: useCallbackOne(
      async ({ uid }: { uid: string }): Promise<number | null> => {
        const snapshot = await leaderboardRef.child(uid).get();
        return snapshot.val();
      },
      []
    ),
    setScore: useCallbackOne(
      ({ uid, score }: { uid: string; score: number }) => {
        console.log('set', { uid, score });
        leaderboardRef.child(uid).set(score);
      },
      [leaderboardRef]
    ),
  };
};

export type EventControlStateLoading = { stage: 'loading' };

export type EventControlStateIntroScreen = {
  stage: 'intro-screen';
};

export type EventControlStateQuiz = {
  stage: 'quiz';
  currentQuestionIndex: number;
};

export type EventControlStateOutro = {
  stage: 'outro';
}

export type EventControlState =
  | EventControlStateLoading
  | EventControlStateIntroScreen
  | EventControlStateQuiz
  | EventControlStateOutro;

export type SetEventControlState = (newState: EventControlState) => void;

/** Note: defaultState MUST be a stable reference */
const useFirebaseState = <STATE>(
  path: string,
  loadingState: STATE,
  defaultState: STATE
) => {
  const fbRef = useFirebaseRef(path);
  const [state, setState] = useState<STATE>(loadingState);

  useEffect(() => {
    const callback = (snapshot: firebase.database.DataSnapshot) => {
      const fbState: STATE | null = snapshot.val();
      if (fbState != null) {
        setState(fbState);
      } else {
        fbRef.set(defaultState);
      }
    };
    fbRef.on('value', callback);

    return () => {
      fbRef.off('value', callback);
    };
  }, [fbRef, defaultState]);

  const setFbState = useCallbackOne(
    (newState: STATE) => {
      // TODO handle errors
      fbRef.set(newState);
    },
    [fbRef]
  );

  return [state, setFbState] as const;
};

const adminControlsDefaultState = {
  stage: 'intro-screen',
  playbackState: 'paused',
} as const;

// This assumes you have already validated that the current user is an admin for the event.
// Otherwise this API will fail when you try to use it.
export const useAdminControls = ({ eventId }: { eventId: string }) => {
  const [eventControlState, setEventControlState] =
    useFirebaseState<EventControlState>(
      `/event-control/${eventId}`,
      {
        stage: 'loading',
      },
      adminControlsDefaultState
    );

  return {
    eventControlState,
    setEventControlState,
  };
};

export const useEventControlState = ({ eventId }: { eventId: string }) => {
  const { eventControlState } = useAdminControls({ eventId });
  return eventControlState;
};

const defaultAnswerState = {
  hasLoaded: true,
  answerIndices: Array(numQuestions).fill(-1), // firebase doesn't like storing `null`s
  answerCorrectIndices: [],
  answers: {},
};
export function useAnswerState({ eventId }: { eventId: string }): {
  answerState: AnswerState;
  setAnswerState: (answerIndices: number[]) => void;
} {
  const auth = useAuth();
  const [answerState, setAnswerState] = useFirebaseState<AnswerState>(
    `/answers/${eventId}/${auth.user!.uid}`,
    {
      hasLoaded: false,
    },
    defaultAnswerState
  );
  return {
    answerState,
    setAnswerState: (answerIndices: number[]) => {
      console.log('setting answer state', answerIndices);

      // Construct a map of textual answers based on the indices
      const answers = answerIndices.map((answerIndex, questionIndex) => {
        const question = questions[questionIndex];

        return {
          index: questionIndex,
          question: question.question,
          hasAnswered: answerIndex >= 0,
          answer: question.answers[answerIndex] ?? '',
          isCorrect: questions[questionIndex].answerIndex === answerIndex,
        }
      });

      setAnswerState({
        hasLoaded: true,
        answerIndices,
        answers,
      });
    }
  };
}

interface LetterState {
    loading: boolean;
    content: string;
    email: string;
}

export function useLetterState({eventId}: { eventId: string }): {
    letterState: LetterState;
    setLetterState: (state: LetterState) => void;
} {
    const auth = useAuth();
    const uid = auth.user?.uid!;
    const email = auth.user?.email!;


    const [letterState, setLetterState] = useFirebaseState<LetterState | null>(
        `/letters/${eventId}/${uid}`,
        null,
        {
            loading: false,
            content: '',
            email: email,
        }
    );
    return {
        letterState: letterState == null ? {
            loading: true,
            content: '',
            email: email,
        } : letterState,
        setLetterState: (letter: LetterState) => {
            setLetterState(letter);
        },
    };
}
