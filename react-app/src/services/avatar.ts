export function getAvatarUrl(md5Hash: string) {
  return `https://www.gravatar.com/avatar/${md5Hash}?d=retro`
}