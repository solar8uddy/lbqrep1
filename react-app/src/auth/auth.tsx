// https://firebase.google.com/docs/auth/web/email-link-auth

import React, {useEffect, useState} from 'react';
import firebase from 'firebase/app';
import {Redirect, useHistory, useLocation, useParams} from 'react-router';

import {useAuth} from './useAuth';
import {useUsers} from '../useFirebase';
import Main from "../components/layout/Main";
import Title from "../components/quiz/Title";
import Logo from "../assets/logo.png";
import styles from './Auth.module.css'
import Input from "../components/form/Input";
import Button from "../components/Button";
import Layout from '../components/layout/Layout';

const actionCodeSettings: firebase.auth.ActionCodeSettings = {
  // URL you want to redirect back to. The domain (www.example.com) for this
  // URL must be in the authorized domains list in the Firebase Console.
  url: `${window.location.origin}/completeSignIn`,
  // This must be true.
  handleCodeInApp: true,
  // iOS: {
  //   bundleId: 'com.example.ios',
  // },
  // android: {
  //   packageName: 'com.example.android',
  //   installApp: true,
  //   minimumVersion: '12',
  // },
  // dynamicLinkDomain: 'example.page.link',
};

const SignInForm = () => {
  const [email, setEmail] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [hasSubmitted, setHasSubmitted] = useState(false);
  const {target} = useParams<{ target?: string }>();

  return (
    <Layout>
      <Main>
        <div>
          <img src={Logo} className={styles.logo} alt="SolarBuddy"/>
        </div>

        {hasSubmitted ? (
          <Title>An email has been sent to your email address to sign in.</Title>
        ) : (
          <form
            onSubmit={(e) => {
              e.preventDefault();
              setIsLoading(true);
              firebase
                .auth()
                .sendSignInLinkToEmail(email, actionCodeSettings)
                .then(() => {
                  // The link was successfully sent. Inform the user.
                  // Save the email locally so you don't need to ask the user for it again
                  // if they open the link on the same device.
                  window.localStorage.setItem('emailForSignIn', email);
                  if (target) {
                    window.localStorage.setItem('target', target);
                  }
                  setHasSubmitted(true);
                })
                .catch((error) => {
                  var errorCode = error.code;
                  var errorMessage = error.message;
                  console.error({errorCode, errorMessage});
                })
                .finally(() => {
                  setIsLoading(false);
                })
            }}
          >
            <Title>Login to join your team</Title>

            <div className={styles.loginEmailInputWrapper}>
              <Input
                type="email"
                name="email"
                placeholder="Email"
                value={email}
                onChange={(e) => {
                  setEmail(e.target.value);
                }}
              />
            </div>

            <Button type="submit" disabled={isLoading}>Login</Button>
          </form>
        )}
      </Main>
    </Layout>
  );
};

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

export const LoginPage = () => {
  const {user} = useAuth();
  const query = useQuery();
  const target = query.get('target');

  useEffect(() => {
    if (!user) {
      if (target) {
        localStorage.setItem('target', target);
      }
    }
  }, [target, user]);

  if (user) {
    if (target) {
      return <Redirect to={target}/>;
    }
    return null;
  }
  return <SignInForm/>;
};

export const CompleteSignInPage = () => {
  const {user, signin} = useAuth();
  const {setProfile} = useUsers();
  const {replace} = useHistory();

  useEffect(() => {
    if (!user && firebase.auth().isSignInWithEmailLink(window.location.href)) {
      // Additional state parameters can also be passed via URL.
      // This can be used to continue the user's intended action before triggering
      // the sign-in operation.
      // Get the email if available. This should be available if the user completes
      // the flow on the same device where they started it.
      let email = window.localStorage.getItem('emailForSignIn');
      if (!email) {
        // User opened the link on a different device. To prevent session fixation
        // attacks, ask the user to provide the associated email again. For example:
        email = window.prompt('Please provide your email for confirmation');
      }
      // The client SDK will parse the code from the link for you.
      firebase
        .auth()
        .signInWithEmailLink(email!, window.location.href)
        .then((result) => {
          // Clear email from storage.
          window.localStorage.removeItem('emailForSignIn');
          console.log('signed in', result);
          signin(result.user!);
          if (result.additionalUserInfo?.isNewUser) {
            setProfile(result.user!.uid!, {
              uid: result.user!.uid!,
              email: result.user!.email!,
              displayName: "",
            });
          }

          return replace('/name');
          // You can access the new user via result.user
          // Additional user info profile not available via:
          // result.additionalUserInfo.profile == null
          // You can check if the user is new or existing:
          // result.additionalUserInfo.isNewUser
        })
        .catch((error) => {
          console.error({error});
          // Some error occurred, you can inspect the code: error.code
          // Common errors could be invalid email and invalid or expired OTPs.
        });
    }
  }, [user, signin, setProfile, replace]);

  return null;
};

export const SetNamePage = () => {
  const {user} = useAuth();
  const {setProfile} = useUsers();
  const {replace} = useHistory();

  const [name, setName] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  if (!user) {
    return <Redirect to={window.localStorage.getItem('target') ?? '/'} />;
  }

  const canSubmit = name.trim().length > 0;

  return (
    <Layout>
      <Main>
        <div>
          <img src={Logo} className={styles.logo} alt="SolarBuddy"/>
        </div>

        <form
          onSubmit={(e) => {
            e.preventDefault();

            if (!canSubmit) {
              return;
            }

            setIsLoading(true);

            setProfile(user.uid, {displayName: name})
              .then(() => {
                setIsLoading(false);

                const target = window.localStorage.getItem('target');

                if (target) {
                  window.localStorage.removeItem('target');
                  return replace(target);
                }

                return replace('/');
              })
              .catch((error) => {
                setIsLoading(false);
                console.error(error);
              });
          }}
        >
          <Title>Enter an avatar name</Title>

          <div className={styles.loginEmailInputWrapper}>
            <Input
              type="text"
              name="email"
              placeholder="Name"
              value={name}
              onChange={(e) => {
                setName(e.target.value);
              }}
            />
          </div>

          <Button type="submit" disabled={isLoading || !canSubmit}>Continue</Button>
        </form>
      </Main>
    </Layout>
  );
};
