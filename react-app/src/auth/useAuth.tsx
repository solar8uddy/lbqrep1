import React, { useState, useEffect, useContext, createContext } from 'react';

// need to load *after* firebase has been initialised
import '../useFirebase';
import firebase from 'firebase/app';
import { useCallbackOne } from 'use-memo-one';

const authContext = createContext<ReturnType<typeof useProvideAuth>>(null!);
// Provider component that wraps your app and makes auth object ...
// ... available to any child component that calls useAuth().
export function ProvideAuth({ children }: { children: React.ReactNode }) {
  const auth = useProvideAuth();
  return <authContext.Provider value={auth}>{children}</authContext.Provider>;
}
// Hook for child components to get the auth object ...
// ... and re-render when it changes.
export const useAuth = () => {
  return useContext(authContext);
};
// Provider hook that creates auth object and handles state
function useProvideAuth() {
  const [user, setUser] = useState<
    { hasLoaded: false } | { hasLoaded: true; user?: firebase.User }
  >({ hasLoaded: false });

  // Wrap any Firebase methods we want to use making sure ...
  // ... to save the user to state.
  const signin = useCallbackOne((user: firebase.User) => {
    setUser({
      hasLoaded: true,
      user,
    });
  }, []);
  const signup = useCallbackOne((email: string, password: string) => {
    return firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then((response) => {
        setUser({
          hasLoaded: true,
          user: response.user ?? undefined,
        });
        return response.user;
      });
  }, []);
  const signout = useCallbackOne(() => {
    return firebase
      .auth()
      .signOut()
      .then(() => {
        setUser({
          hasLoaded: true,
        });
      });
  }, []);

  // Subscribe to user on mount
  // Because this sets state in the callback it will cause any ...
  // ... component that utilizes this hook to re-render with the ...
  // ... latest auth object.
  useEffect(() => {
    const unsubscribe = firebase.auth().onAuthStateChanged((user) => {
      console.log('auth state changed', user);
      setUser({
        hasLoaded: true,
        user: user ?? undefined,
      });
    });
    return () => unsubscribe();
  }, []);
  return {
    isLoading: !user.hasLoaded,
    user: (user.hasLoaded && user.user) || null,
    signin,
    signup,
    signout,
  };
}
